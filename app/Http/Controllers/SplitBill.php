<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TempBill;
use App\Models\Bill;
use App\Models\BillDetail;
use DataTables;

class SplitBill extends Controller
{
    public function split(Request $request)
    {
      $javascript = 'split.js';
      return view('split',compact('javascript'));
    }

    public function getTempBill(Request $request)
    {

        if ($request->ajax()) {
            $data = TempBill::getTemp();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('total', function($row){
                    return "Rp. ".number_format($row->total,0,",",".");
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="d-flex gap-3">
                                      <a onclick="hapus(\''.$row->fullname.'\')" href="javascript:void(0);" class="text-danger"><i class="mdi mdi-delete font-size-18"></i></a>
                                  </div>';
                    return $actionBtn;
                })
                ->addColumn('detail', function($row){
                    $detail = '<button onclick="detail(\''.$row->fullname.'\')" type="button" class="btn btn-primary btn-sm btn-rounded">
                                      Detail pesanan
                                  </button>';
                    return $detail;
                })
                ->rawColumns(['action','detail'])
                ->make(true);
        }
    }

    public function addTemp(Request $request)
    {
      $data = $request->all();
      $fullname = $data['outer-group'][0]['fullname'];
      foreach($data['outer-group'][0]['inner-group'] as $ls){
        $param['fullname'] = $fullname;
        $param['item_name'] = $ls['item_name'];
        $param['qty'] = $ls['qty'];
        $param['price'] = $ls['price'];
        $param['subtotal'] = $ls['qty'] * $ls['price'];
        TempBill::Create($param);
      }

    }

    public function dellTemp(Request $request)
    {
      $id = $request->id;
      TempBill::where('fullname',$id)->delete();
      echo 1;
    }

    public function detailTemp(Request $request)
    {
      $data = TempBill::orderName($request->id);
      return view('splitDetail',compact('data'));
    }

    public function saveTranscation(Request $request)
    {
      $sumTemp = TempBill::sum('subtotal');
      $discount = ($sumTemp * $request->discount) / 100;
      $shipping = $request->shipping;


      $total = $sumTemp - $discount + $shipping;

      $ins['transaction_date'] = date("Y-m-d H:i:s");
      $ins['total'] = $sumTemp;
      $ins['discount'] = $request->discount;
      $ins['shipping_cost'] = $shipping;
      $ins['grand_total'] = $total;

      $id = Bill::insertGetId($ins);

      $getTemp = TempBill::get();

      foreach($getTemp as $ls){
        $det['bill_id'] = $id;
        $det['fullname'] = $ls['fullname'];
        $det['item_name'] = $ls['item_name'];
        $det['price'] = $ls['price'];
        $det['qty'] = $ls['qty'];


        $subtotal_det = $ls['price'] * $ls['qty'];
        $discount_det = ($subtotal_det * $request->discount) / 100;
        $total_det = $subtotal_det - $discount_det;

        $det['discount'] = $discount_det;
        $det['subtotal'] = $total_det;
        BillDetail::Create($det);
        TempBill::where('temp_id',$ls['temp_id'])->delete();
      }

    }

    public function listBill(Request $request)
    {

      $javascript = 'listBill.js';
      return view('listBill',compact('javascript'));
    }

    public function getBill(Request $request)
    {

        if ($request->ajax()) {
            $data = Bill::get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('transaction_date', function($row){
                    return date("d F Y",strtotime($row->transaction_date));
                })
                ->addColumn('discount', function($row){
                    return $row->discount."%";
                })
                ->addColumn('grand_total', function($row){
                    return "Rp. ".number_format($row->grand_total,0,",",".");
                })
                ->addColumn('detail', function($row){
                    $url = url('/detail-bill/'.$row->bill_id);
                    $detail = '<a target="_blank" href="'.$url.'" type="button" class="btn btn-primary btn-sm btn-rounded">
                                      Detail pesanan
                                  </a>';
                    return $detail;
                })
                ->rawColumns(['detail'])
                ->make(true);
        }
    }

    public function detailBill(Request $request)
    {

      $javascript = 'detailBill.js';
      $id = $request->id;
      return view('detailBill',compact('javascript','id'));
    }

    public function detailBillPerson(Request $request)
    {
      if ($request->ajax()) {
        $id = $request->id;
          $data = BillDetail::getBillId($id);
          $shipping = Bill::getShipp($id);
          $people = count($data);
          $shipping_cost = $shipping->shipping_cost / $people;
          $discount = $shipping->discount;
          return Datatables::of($data)
              ->addIndexColumn()
              ->addColumn('total', function($row) use($shipping_cost){
                  $total = $row->total + $shipping_cost;
                  return "Rp. ".number_format($total,0,",",".");
              })
              ->addColumn('detail', function($row) use($shipping_cost,$discount) {
                $total = $row->total + $shipping_cost;
                $detail = '<button onclick="detail(\''.$row->fullname.'\',\''.$shipping_cost.'\',\''.$discount.'\',\''.$total.'\')" type="button" class="btn btn-primary btn-sm btn-rounded">
                                  Detail pesanan
                              </button>';
                return $detail;
              })
              ->rawColumns(['detail'])
              ->make(true);
      }
    }

    public function detailBillByPerson(Request $request)
    {
      $data = BillDetail::orderName($request->id);
      $shipping = $request->shipping;
      $discount = $request->discount;
      $grand_total = $request->grand_total;
      return view('splitDetailBill',compact('data','shipping','discount','grand_total'));
    }
}
