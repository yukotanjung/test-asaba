<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class BillDetail extends Model
{
    use HasFactory;
    protected $primaryKey = "bill_detail_id";
    protected $table = "tbl_bill_detail";
    protected $guarded = [];
    public $timestamps = false;

    public function scopegetBillId($query,$id)
    {

      $data = $query->select('tbl_bill_detail.*',DB::raw("SUM(subtotal) AS total"))
              ->where('bill_id',$id)
              ->groupBy('fullname')->get();
      return $data;
    }

    public function scopeorderName($query,$id)
    {
      $data = $query->where('fullname',$id)->get();
      return $data;
    }
}
