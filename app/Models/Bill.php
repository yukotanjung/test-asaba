<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Bill extends Model
{
    use HasFactory;
    protected $primaryKey = "bill_id";
    protected $table = "tbl_bill";
    protected $guarded = [];
    public $timestamps = false;

    public function scopegetShipp($query,$id)
    {
      $data = $query->where('bill_id',$id)->first();
      return $data;
    }
}
