<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class TempBill extends Model
{
    use HasFactory;
    protected $primaryKey = "temp_id";
    protected $table = "tbl_bill_temp";
    protected $guarded = [];
    public $timestamps = false;

    public function scopegetTemp($query)
    {
      $data = $query->select('tbl_bill_temp.*',DB::raw("SUM(subtotal) AS total"))
              ->groupBy('fullname')->get();
      return $data;
    }

    public function scopeorderName($query,$id)
    {
      $data = $query->where('fullname',$id)->get();
      return $data;
    }


}
