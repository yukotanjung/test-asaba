<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SplitBill;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SplitBill::class, 'split']);
Route::get('getbill', [SplitBill::class, 'getTempBill']);
Route::post('add-temp', [SplitBill::class, 'addTemp']);
Route::post('dell-temp',[SplitBill::class, 'dellTemp']);
Route::post('detail-temp',[SplitBill::class, 'detailTemp']);
Route::post('save-transc',[SplitBill::class, 'saveTranscation']);
Route::get('list-bill',[SplitBill::class, 'listBill']);
Route::get('mainBill', [SplitBill::class, 'getBill']);
Route::get('detail-bill/{id}',[SplitBill::class, 'detailBill']);
Route::get('getbill-detail/{id}',[SplitBill::class, 'detailBillPerson']);
Route::post('detail-bill-person',[SplitBill::class, 'detailBillByPerson']);
