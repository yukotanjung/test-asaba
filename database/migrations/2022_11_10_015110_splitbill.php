<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Splitbill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tbl_bill', function(Blueprint $table)
        {
            $table->increments('bill_id', 11);
            $table->dateTime('transaction_date');
            $table->integer('total')->unsigned();
            $table->integer('discount')->unsigned();
            $table->integer('shipping_cost')->unsigned();
            $table->integer('grand_total')->unsigned();
        });

        Schema::create('tbl_bill_detail', function(Blueprint $table)
        {
            $table->increments('bill_detail_id', 11);
            $table->integer('bill_id')->unsigned();
            $table->string('fullname',255);
            $table->string('item_name',255);
            $table->integer('price')->unsigned();
            $table->integer('qty')->unsigned();
            $table->double('discount')->unsigned();
            $table->integer('shipping_cost')->unsigned();
            $table->integer('subtotal')->unsigned();
        });

        Schema::create('tbl_bill_temp', function(Blueprint $table)
        {
            $table->increments('temp_id', 11);
            $table->string('fullname',255);
            $table->string('item_name',255);
            $table->integer('price')->unsigned();
            $table->integer('qty')->unsigned();
            $table->integer('subtotal')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('tbl_bill');
      Schema::dropIfExists('tbl_bill_detail');
      Schema::dropIfExists('tbl_bill_temp');
    }
}
