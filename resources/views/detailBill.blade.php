@extends('template')
@section('content')
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Split Bill</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                                <li class="breadcrumb-item active">Detail Bill</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

        </div> <!-- container-fluid -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                      <input type="hidden" id="bill_id" name="" value="{{$id}}">
                      <table id="table-detail" class="table align-middle table-nowrap table-check">
                          <thead class="table-light">
                              <tr>

                                  <th class="align-middle">Nama</th>
                                  <th class="align-middle">Total</th>
                                  <th class="align-middle">Detail</th>
                              </tr>
                          </thead>
                          <tbody>


                          </tbody>
                      </table>



                    </div>
                </div>






            </div>
        </div>
    </div>
    <!-- End Page-content -->
</div>

<div class="modal fade orderdetailsModal" tabindex="-1" role="dialog" aria-labelledby=orderdetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=orderdetailsModalLabel">Order Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <div class="table-responsive">
                    <table class="table align-middle table-nowrap">
                        <thead>
                            <tr>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Harga</th>
                            </tr>
                        </thead>
                        <tbody id="temp-data">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
