@extends('template')
@section('content')
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">Split Bill</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Menu</a></li>
                                <li class="breadcrumb-item active">Split Bill</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end page title -->

        </div> <!-- container-fluid -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <h4 class="card-title">Split Bill</h4>

                        <form class="outer-repeater" id="form_temp">
                            <div data-repeater-list="outer-group" class="outer">
                                <div data-repeater-item class="outer">
                                    <div class="form-group row mb-4">
                                        <label for="taskname" class="col-form-label col-lg-2">Nama</label>
                                        <div class="col-lg-10">
                                            <input id="taskname" name="fullname" type="text" class="form-control" placeholder="Nama">
                                        </div>
                                    </div>

                                    <div class="inner-repeater mb-4">
                                        <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                            <label class="col-form-label col-lg-2">Tambah Order Pesanan</label>
                                            <div  data-repeater-item class="inner col-lg-10 ms-md-auto">
                                                <div class="mb-3 row align-items-center">
                                                    <div class="col-md-3">
                                                        <input type="text" name="item_name[]" class="inner form-control" placeholder="Nama barang"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" name="qty[]"  class="inner form-control" placeholder="Jumlah"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input name="price[]" type="text" class="inner form-control" placeholder="Harga"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="mt-2 mt-md-0 d-grid">
                                                            <input data-repeater-delete type="button" class="btn btn-danger inner" value="Hapus"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="col-lg-10">
                                                <input data-repeater-create type="button" class="btn btn-warning inner" value="Tambah pesanan"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-wrap gap-2">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Simpan</button>
                            </div>
                        </form>



                    </div>
                </div>

                <div class="row">
                  <div class="col-xl-8">
                    <table id="table-temp" class="table align-middle table-nowrap table-check">
                        <thead class="table-light">
                            <tr>

                                <th class="align-middle">Nama</th>
                                <th class="align-middle">Total</th>
                                <th class="align-middle">Detail</th>
                                <th class="align-middle">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                  </div>

                  <div class="col-xl-4">

                       <div class="card">
                          <div class="card-body">

                              <div class="table-responsive">
                                  <table class="table mb-0">
                                      <tbody>

                                          <tr>
                                              <td style="vertical-align: middle;">Discount</td>
                                              <td>
                                                <input type="text" class="form-control" id="discount" value="" placeholder="%">
                                              </td>
                                          </tr>
                                          <tr>
                                              <td style="vertical-align: middle;">Ongkos Kirim</td>
                                              <td>
                                                <input type="text" class="form-control" id="shipping" value="">
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                                  <div class="text-sm-end mt-4">
                                      <button onclick="simpan()" class="btn btn-success">
                                      <i class="mdi mdi-cart-arrow-right me-1"></i> Checkout </button>
                                  </div>
                              </div>
                              <!-- end table-responsive -->
                          </div>
                      </div>
                      <!-- end card -->
                  </div>
                </div>




            </div>
        </div>
    </div>
    <!-- End Page-content -->
</div>

<div class="modal fade orderdetailsModal" tabindex="-1" role="dialog" aria-labelledby=orderdetailsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id=orderdetailsModalLabel">Order Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <div class="table-responsive">
                    <table class="table align-middle table-nowrap">
                        <thead>
                            <tr>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                            </tr>
                        </thead>
                        <tbody id="temp-data">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
