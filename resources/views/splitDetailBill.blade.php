@php
$total = 0
@endphp


@foreach ($data as $ls)
  @php
  $total += $ls->price * $ls->qty
  @endphp
<tr>
    <td>
        <div>
            <h5 class="text-truncate font-size-14">{{$ls->item_name}}</h5>
            <p class="text-muted mb-0">Rp. {{ number_format($ls->price,0,",",".") }} x {{$ls->qty}}</p>
        </div>
    </td>
    <td>Rp. {{ number_format($ls->price * $ls->qty,0,",",".") }}</td>
</tr>
@endforeach
<tr>
    <td>
        <h6 class="m-0 text-right">Sub Total:</h6>
    </td>
    <td>
      Rp. {{ number_format($total,0,",",".") }}
    </td>
</tr>
<tr>
    <td>
        <h6 class="m-0 text-right">Diskon:</h6>
    </td>
    <td>
      Rp. {{ number_format($discount,0,",",".") }}%
    </td>
</tr>
<tr>
    <td>
        <h6 class="m-0 text-right">Ongkos kirim:</h6>
    </td>
    <td>
      Rp. {{ number_format($shipping,0,",",".") }}
    </td>
</tr>
<tr>
    <td>
        <h6 class="m-0 text-right">Grand Total:</h6>
    </td>
    <td>
      Rp. {{ number_format($grand_total,0,",",".") }}
    </td>
</tr>
