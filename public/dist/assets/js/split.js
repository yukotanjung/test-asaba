$(document).ready(function() {
  var table = $('#table-temp').DataTable({
      processing: true,
      serverSide: true,
      ajax: hostname+"/getbill",
      searching: false,
      lengthChange: false,
      bLengthChange: false,
      pagingType: "numbers",
      language: {
        emptyTable: "<p class='text-center'>Data tidak ada</p>",
      },
      columns: [

          {data: 'fullname', name: 'fullname'},
          {data: 'total', name: 'total'},
          {data: 'detail', name: 'detail'},
          {
              data: 'action',
              name: 'action',
          },
      ]
  });

  $("#form_temp").submit(function(event) {
    event.preventDefault();
    $.ajax({
          type: "POST",
          url: hostname + '/add-temp',
          data:$("#form_temp").serialize(),
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(data) {
            $(":input", "#form_temp")
      					.not(":button, :submit, :reset, :hidden")
      					.val("")
      					.prop("checked", false)
      					.prop("selected", false);
            table.ajax.reload(null,false);
          }
      });

  });
});


  function hapus(id){
	Swal.fire({
						icon: "warning",
						title: "Apakah anda yakin ingin menghapusnya?",
						showCancelButton: true,
						confirmButtonText: "Ya",
						cancelButtonText: "Batal",
					}).then((result) => {
						if (result.isConfirmed) {
							$.ajax({
								type: "POST",
								url: hostname + "/dell-temp",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
								data: {id:id},
								success: function (data) {
                  $('#table-temp').DataTable().ajax.reload();
									Swal.fire("Berhasil!", "Berhasil menghapus data.", "success");
								},
							});
						}
					});
}

function detail(id) {
  $.ajax({
    type: "POST",
    url: hostname + "/detail-temp",
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {id:id},
    success: function (data) {
      $('#temp-data').html(data);
      $(".orderdetailsModal").modal('show');
    },
  });

}

function simpan() {
  discount = $("#discount").val();
  shipping = $("#shipping").val();
  $.ajax({
    type: "POST",
    url: hostname + "/save-transc",
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {discount:discount,shipping:shipping},
    success: function (data) {
      $("#discount").val('');
      $("#shipping").val('');
      Swal.fire("Berhasil!", "Berhasil simpan transaksi.", "success");
      window.location.reload();
    },
  });
}
