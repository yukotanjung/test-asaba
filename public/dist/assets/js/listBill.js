$(document).ready(function() {
  var table = $('#table-bill').DataTable({
      processing: true,
      serverSide: true,
      ajax: hostname+"/mainBill",
      searching: false,
      lengthChange: false,
      bLengthChange: false,
      pagingType: "numbers",
      language: {
        emptyTable: "<p class='text-center'>Data tidak ada</p>",
      },
      columns: [

          {data: 'transaction_date', name: 'transaction_date'},
          {data: 'discount', name: 'discount'},
          {data: 'grand_total', name: 'grand_total'},
          {data: 'detail', name: 'detail'},
      ]
  });
});
