$(document).ready(function() {
  bill_id = $("#bill_id").val();
  var table = $('#table-detail').DataTable({
      processing: true,
      serverSide: true,
      ajax: hostname+"/getbill-detail/"+bill_id,
      searching: false,
      lengthChange: false,
      bLengthChange: false,
      pagingType: "numbers",
      language: {
        emptyTable: "<p class='text-center'>Data tidak ada</p>",
      },
      columns: [

          {data: 'fullname', name: 'fullname'},
          {data: 'total', name: 'total'},
          {data: 'detail', name: 'detail'},
      ]
  });
});

function detail(id,shipping,discount,grand_total) {
  $.ajax({
    type: "POST",
    url: hostname + "/detail-bill-person",
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    data: {id:id,shipping:shipping,discount:discount,grand_total:grand_total},
    success: function (data) {
      $('#temp-data').html(data);
      $(".orderdetailsModal").modal('show');
    },
  });

}
